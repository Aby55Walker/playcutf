

export default class Fotoramasetup {
  constructor() {

    // checkbox
    document.querySelectorAll('.form-btn').forEach((btn, i) => {

      let ckeckbox = document.querySelector('.form__hidden-checkbox');
      let checkboxText = document.querySelectorAll('.form__text');
      btn.addEventListener('click', e => {
        if (!ckeckbox.checked) {
          e.preventDefault();
          checkboxText[i].style.color = 'red';
        } else {
          checkboxText[i].style = '';
        }
      });
    });



    $(window).bind('scroll',function(e){
      parallaxScroll();
    });

    window.addEventListener('resize', parallaxScroll);

    function parallaxScroll(){
      if (window.innerWidth < 1201) {
        document.querySelectorAll('.bg-circle__item').forEach(item => {
          item.style = '';
        });
        return;
      }
      let coef1 = 0.01;
      let coef2 = 0.020;
      let coef3 = 0.015;
      let coef4 = 0.005;

      var scrolled = $(window).scrollTop();
      $('.bg-circle__item2').css('top',(  - 73 -  (scrolled * coef1))+'%');
      $('.bg-circle__item3').css('top',(    10 -  (scrolled * coef2))+'%');
      $('.bg-circle__item4').css('top',(    90 -  (scrolled * coef3))+'%');
      $('.bg-circle__item5').css('top',(   132 -  (scrolled * coef4))+'%');
    }

    $('.fotorama__box').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: true,
      dotsClass: 'dots',
      rows: 0,
      arrows: true,
    });
  }
}