'use strict';

import $ from 'jquery';

export default class Cut_and_play {
	constructor() {
		window.addEventListener('load', () => {
			this.showPhoto();
			this.toggleTab();
			this.boysSliderInit();
		});
	}
	// переключает табы девочки/мальчики
	toggleTab() {
		let btns = document.querySelectorAll('.catalog-btn');

		btns.forEach( btn => {
			btn.addEventListener('click', () => {

				let tabIndex = btn.getAttribute('data-tab');
				document.querySelector('.catalog-btn.active').classList.remove('active');
				btn.classList.add('active');

				document.querySelector('.catalog__imgs-box.active').classList.remove('active');
				document.querySelector('.catalog__imgs-box[data-tab="' + tabIndex + '"]').classList.add('active');

			});
		});
	}
	//переключет фотки
	showPhoto() {
		let photos = document.querySelectorAll('.catalog__img');


		photos.forEach(photo => {

			photo.addEventListener('click', e => {

				let imgPlace = document.querySelector('.catalog__imgs-box.active .catalog__card-img');
				let imgDesc = document.querySelector('.catalog__imgs-box.active .catalog__card-desc');

				let src = photo.getAttribute('data-src');
				imgPlace.style.background = `url(images/${src}) center no-repeat`;
				imgPlace.style.backgroundSize = `cover`;
				imgDesc.textContent = photo.getAttribute('data-desc');
				if (document.querySelector('.catalog__img.active')) {
					document.querySelector('.catalog__img.active').classList.remove('active');
				}
				photo.classList.add('active')

			});
		});
	}

	boysSliderInit() {
		if (window.innerWidth < 768) {
			$('.catalog__boys-slider').slick({
				infinite: true,
				slidesToShow: 1,
				slidesToScroll: 1,
				dots: false,
				arrows: false,
				rows: 0,
			});
		}
	}
}