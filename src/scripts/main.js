'use strict';
import $ from 'jquery';
import '../modules/site/datapicker';

import '@babel/polyfill';
import Icon             from '../modules/icon/icon';
import Burger           from '../modules/burger/burger';
import Modal            from '../modules/modal/modal';
import FotoramaSetup    from '../modules/fotorama/fotorama_setup';
import Cut_and_play    from '../modules/cut_and_play/cut_and_play';


new Icon();
new Burger();
new Modal();
new FotoramaSetup();
new Cut_and_play();

// mask
import Inputmask from "inputmask";
const date = document.getElementById("mask");
const inputMaslPhone = new Inputmask("+7 999 99 99");
inputMaslPhone.mask(date);

const time = document.getElementById("time");
const inputMaskTime = new Inputmask("99:99");
inputMaskTime.mask(time);
////